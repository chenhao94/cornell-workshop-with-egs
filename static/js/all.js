/*** configuration ***/
var host_prefix = 'http://si023.systems.cs.cornell.edu:443';
var n_gram_ajax_site = host_prefix + '/n-gram-query';
var author_ajax_site = host_prefix + '/authorship-query';
var send_timeout = 20000;
var retry_rate = 5000;
var format_count = d3.format(",.0f");
var format_date = d3.time.format("%Y-%m-%d");

var month_name = ["Jan", "Feb", "Mar", "Apr",
                "May", "Jun", "Jul", "Aug",
                "Sep", "Oct", "Nov", "Dec"];
var day_name = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

/* n-gram graph configuration */
var n_gram_margin = {top: 50, right: 50, bottom: 80, left: 50},
    n_gram_height = 300 - n_gram_margin.top - n_gram_margin.bottom;
var n_gram_bin_num = 30;

var n_gram_info_page_size = 10;
var n_gram_info_buffer = ["buffer0", "buffer1"];
var n_gram_min_date = "1970-01-01";
var n_gram_max_date = "2015-01-01";

/* authorship graph configuration */
var author_margin = n_gram_margin,
    author_height = 600 - author_margin.top - author_margin.bottom;

/*** helper functions ***/
function Blocker(sleeper) {
    this.counter = 0;
    this.waiting = false;
    this.sleeper = sleeper;
}

Blocker.prototype.setHook = function() {
    var blocker = this;
    return function() {
        blocker.counter++;
    }
}
Blocker.prototype.setNotifier = function(cb) {
    var blocker = this;
    return function() {
        if (cb)
            cb.apply(this, arguments);
        if (--blocker.counter == 0 && blocker.waiting)
            blocker.sleeper();
    }
};

Blocker.prototype.go = function() {
    if (this.counter == 0)
        this.sleeper();
    this.waiting = true;
}

function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
}

function hex_to_rgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

var line_colors = d3.scale.category10();
function get_color(idx) {
    console.log(idx);
    var rgb = hex_to_rgb(line_colors(idx % 10));
    rgb.r = (rgb.r + 255) / 2.0;
    rgb.g = (rgb.g + 255) / 2.0;
    rgb.b = (rgb.b + 255) / 2.0;
    return '#' + hex(rgb.r) + hex(rgb.g) + hex(rgb.b);
}

function max_date(prev_val, cur_val, index, array) {
    return Math.max(prev_val, cur_val.date);
}

function min_date(prev_val, cur_val, index, array) {
    return Math.min(prev_val, cur_val.date);
}

Date.prototype.toUTCDateString = function () {
    var date = this.getUTCDate();
    date = date > 9 ? date: "0" + date;
    return day_name[this.getUTCDay()] + " " +
        month_name[this.getUTCMonth()] + " " +
         date + " " + this.getUTCFullYear();
}

var svg; /* canvas */

var n_gram_width;
function n_gram_setup() {
    var canvas = d3.select("#canvas");
    n_gram_width = parseInt(canvas.style("width")) - n_gram_margin.left - n_gram_margin.right;
    /* setup svg */
    svg = canvas.append("svg")
                .attr("width", n_gram_width + n_gram_margin.left + n_gram_margin.right)
                .attr("height", n_gram_height + n_gram_margin.top + n_gram_margin.bottom)
                .append("g")
                .attr("transform", "translate(" + n_gram_margin.left + "," + n_gram_margin.top + ")");

    svg.append("g")
        .attr("id", "x-axis")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + n_gram_height + ")");

    svg.append("g")
        .attr("id", "y-axis")
        .attr("class", "y axis");

    svg.append("g")
        .attr("id", "bars");
    
    svg.append("g")
        .attr("id", "lines");
}

function n_gram_preprocessing(raw_data) {
    raw_data.map(function (cur_val, index, array) {
                    cur_val.date = new Date(cur_val.date);
                    return cur_val;
                });
    return raw_data;
}

var n_gram_info_data;
var n_gram_info_page_base;

function n_gram_info_fill(buffer_name, d) {
    var table = $("#" + buffer_name + " > table");
    table.html('');
    d.map(function(dd) {
        table.append("<tr><td>" + dd.title + "</td><td>" +
                        dd.date.toUTCDateString() + "</td></tr>");
    });
}

function n_gram_info_setup(d) {
    n_gram_info_buffer.forEach(function (cur_val, index, array) {
        var buffer = $('#' + cur_val);
        buffer.replaceWith("<div id='" + cur_val +
                           "' style='height: 0; overflow-x: hidden;" +
                           " overflow-y: auto; position: absolute'>" +
                     "<table class='table table-striped table-hover'></table>" +
                     "</div>");
        $('#buffers').css('display', 'none');
        $('.pager').css('display', 'none');
    });
}

function n_gram_info_render(d) {
    var buffer_name = n_gram_info_buffer[0];
    var buffer = $('#' + buffer_name);
    n_gram_info_page_base = 0;
    n_gram_info_fill(buffer_name, d.slice(n_gram_info_page_base,
                                            n_gram_info_page_size));

    $('#buffers').css('display', 'block');
    $('.pager').css('display', 'block');
    buffer.css('height','auto')
            .data('new_height',buffer.height())
    var buffer_width = $("#buffer_wrapper").width();
    var buffer_height = buffer.data('new_height');
    $("#buffers").css('width', buffer_width)
                .css('height', buffer_height);
    buffer.css('width', buffer_width);
    $('.pager').css('opacity', 0).stop(true);
    var b = new Blocker(function () {
        $('.pager').animate({ opacity: 1 }, 400);
    });
    b.setHook();
    buffer.height(0)
            .animate({ height: buffer_height}, 400, "swing", b.setNotifier());
    b.go();
    n_gram_info_data = d;
    function slide(dir) {
        return function () {
            dir = dir > 0 ? 1 : -1;
            var new_base = n_gram_info_page_base + dir * n_gram_info_page_size;
            if (new_base < n_gram_info_data.length && new_base >= 0)
            {
                old_buffer_name = n_gram_info_buffer[0];
                new_buffer_name = n_gram_info_buffer[1];
                old_buffer = $("#" + old_buffer_name);
                new_buffer = $("#" + new_buffer_name);
                n_gram_info_fill(new_buffer_name,
                                   n_gram_info_data.slice(new_base,
                                        new_base + n_gram_info_page_size));
                n_gram_info_page_base = new_base;
                new_buffer.css('height', buffer_height)
                        .css('width', buffer_width)
                        .css('left', dir * buffer_width);
                var b = new Blocker(function () {
                    var t = n_gram_info_buffer[0];
                    n_gram_info_buffer[0] = n_gram_info_buffer[1];
                    n_gram_info_buffer[1] = t;
                });
                b.setHook()();
                b.setHook()();
                old_buffer.animate({left: dir * -buffer_width}, 300, "swing", b.setNotifier());
                new_buffer.animate({left: 0}, 300, "swing", b.setNotifier());
                b.go();
            }
            return false;
        };
    }
    $(".pager .next").unbind("click").on("click", slide(1));
    $(".pager .previous").unbind("click").on("click", slide(-1));
}

function n_gram_render(phrases, papers) {
    n_gram_info_setup();
    if (papers.length < 1)
        return;
    /*
    var start_date = new Date(papers.reduce(min_date, papers[0].date));
    var end_date = new Date(papers.reduce(max_date, papers[0].date));
    */
    var start_date = new Date(n_gram_min_date);
    var end_date = new Date(n_gram_max_date);
    var bin_gap = Math.floor((end_date - start_date) / n_gram_bin_num);
    var bin_thresholds = [start_date];

    for (var i = 0, prev = start_date.getTime(), now;
         i < n_gram_bin_num; i++, prev = now)
        bin_thresholds.push(new Date(now = prev + bin_gap));

    var x = d3.time.scale()
            .domain([start_date, end_date])
            .range([0, n_gram_width]);

    var hists = papers.map(function (cur_val, index, array) {
                        return d3.layout.histogram()
                            .value(function (d) { return d.date; })
                            .bins(bin_thresholds)(cur_val)
                            .map(function (cur_val, index, array) {
                                cur_val.pos = index;
                                return cur_val;
                            });
                        });

    var sum_data = [];
    for (var i = 0; i < hists[0].length; i++)
    {
        var bin = [];
        bin.x = hists[0][i].x;
        bin.y = 0;
        bin.dx = hists[0][i].dx;
        bin.pos = i;
        for (var j = 0; j < hists.length; j++)
        {
            var b = hists[j][i];
            bin.y += b.y;
            for (var k = 0; k < b.length; k++)
                bin.push(b[k]);
        }
        sum_data.push(bin);
    }

    svg.append("rect")
        .attr("class", "overlay")
        .attr("width", n_gram_width)
        .attr("height", n_gram_height);
        /*
        .on("mousemove", function() {
            var bin_id = Math.floor((x.invert(d3.mouse(this)[0]) - start_date) / bin_gap);
            if (!(0 <= bin_id && bin_id < bin_num))
                return;
            var bin = sum_data[bin_id];
            if (bin.y == 0)
                return;
            n_gram_info_render(bin);
        });
       */

    var x_axis = d3.svg.axis()
                    .scale(x).ticks(5)
                    .orient("bottom").tickFormat(format_date);

    var hist_fdata = sum_data.filter(function (cur_val, index, array) {
                        return cur_val.y > 0;
                    });

    /* begin bar chart */
    var svg_bar = svg.select("#bars");
    var bar = svg_bar.selectAll(".bar")
                    .data(hist_fdata, function(d) { return d.pos; });
    if (hist_fdata.length > 0)
    {
        var bar_y = d3.scale.linear()
                        .domain([0, d3.max(hist_fdata, function(d) { return d.y; })])
                        .range([n_gram_height, 0]);

        var bin_width = x(new Date(start_date.getTime() + hist_fdata[0].dx)) * 0.5;
        var bar_enter = bar.enter()
                    .append("g")
                    .attr("class", "bar")
                    .attr("transform", function(d) {
                        return "translate(" + x(d.x) + "," + bar_y(0) + ")";
                    })
                    .each(function (d) {
                        sum_data[d.pos].ref = this;
                    })
                    .on("mouseover", n_gram_info_render);

        bar_enter.append("rect")
            .attr("x", 1)
            .attr("y", -1)
            .attr("width", bin_width)
            .attr("height", function(d) { return n_gram_height - bar_y(0); });

        bar.select("rect")
            .transition()
            .duration(300)
            .attr("width", bin_width)
            .attr("height", function(d) { return n_gram_height - bar_y(d.y); });

        bar.transition()
            .duration(300)
            .attr("transform", function(d) {
                    return "translate(" + x(d.x) + "," + bar_y(d.y) + ")";
                });


        bar_enter.append("text")
            .attr("dy", "-.75em")
            .attr("y", 6)
            .attr("x", bin_width / 2)
            .attr("text-anchor", "middle")
            .text(function(d) { return format_count(d.y); });

        bar.select("text")
            .attr("x", bin_width / 2)
            .attr("text-anchor", "middle")
            .text(function(d) { return format_count(d.y); });

    }
    bar.exit().remove();
    /* end bar chart */

    /* begin line chart */
    var svg_lines = svg.select("#lines");
    var line = svg_lines.selectAll(".line")
                .data(hists);
    var line_y = d3.scale.linear()
                    .domain([0, d3.max($.map(hists, function (data) {
                                return d3.max(data, function(d) { return d.y; }); }))],
                        function(d) { return d.y; })
                    .range([n_gram_height, 0]);

    var y_axis = d3.svg.axis().scale(line_y).ticks(5).orient("left").tickFormat(format_count);

    var valueline = d3.svg.line()
        .x(function (d) { return x(d.x) + bin_width / 2.0; })
        .y(function (d) { return line_y(d.y); });
    var enter_blocker = new Blocker(function() {});
    var line_enter = line.enter()
                        .append("g")
                        .attr("class", "line")
                        .append("path")
                        .attr("opacity", "0");

    line.select("path")
        .transition()
        .duration(300)
        .attr("d", function (d) { return valueline(d); })
        .attr("stroke", function (d, i) {
            var color = get_color(i);
             d.color = color;
             return color;
        })
        .each(function (d) {
            d.line = this;
        })
        .each(enter_blocker.setHook())
        .each("end", enter_blocker.setNotifier(function () {
            line_enter.transition()
                        .duration(300)
                        .attr("opacity", "1")
        }));

    line.exit()
        .attr("opacity", "1")
        .transition()
        .duration(300)
        .attr("opacity", "0")
        .remove();
    /* end line chart */

    /* begin legend */
    var ldata = [];
    for (var i = 0; i < hists.length; i++)
        ldata.push({ phrase: phrases[i].join(' '),
                    color: hists[i].color,
                    line: hists[i].line });
    console.log(hists);
    var legends = d3.select("#legends")
                    .selectAll(".legend").data(ldata);
    legends.enter()
            .append("span")
            .attr("class", "legend label label-default")
            .style("margin-left", "2px")
            .on("mouseover", function (d) {
                d3.select(d.line).attr("class", "selected");
            })
            .on("mouseout", function (d) {
                d3.select(d.line).attr("class", "");
            });

    legends.style("color", function (d) { return d.color; })
            .text(function (d) { return d.phrase; });

    legends.exit().remove();
    /* end legend */

    /* update axes */
    svg.select("#x-axis")
        .transition()
        .duration(300).call(x_axis);
    svg.select("#y-axis")
        .transition()
        .duration(300).call(y_axis);
}

//render(values);

function send_request(url, data, on_success) {
    var btn = $("#submit");
    btn.prop("disabled", true);
    btn.html("Wait");
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        async: true,
        data: data,
        success:
            function(resp) {
                on_success(resp);
                btn.prop("disabled", false);
                btn.html("Search");
            },
        timeout: send_timeout,
        error: function(a, b, c) {
            console.log("failed while connecting, reason: " + b);
            setTimeout(function() {
                send_request(url, data, on_success);
            }, retry_rate);
        }
    });
}

function n_gram_search(phrases) {
    phrases = phrases.split(',').map(function (cur_val, index, array) {
                    return cur_val.trim().split(" ");
                });

    var waiting = phrases.length;
    var resps = [];
    phrases.forEach(function (cur_val, index, array) {
        send_request(n_gram_ajax_site, { words: JSON.stringify(cur_val)},
                 function(resp) {
                     console.log("response");
                     console.log(resp);
                     resps[index] = n_gram_preprocessing(resp);
                     if (--waiting == 0)
                         n_gram_render(phrases, resps);
                 })});
}

var author_width;
function author_setup() {
    var canvas = d3.select("#canvas");
    author_width = parseInt(canvas.style("width")) - author_margin.left - author_margin.right;
    /* setup svg */
    svg = canvas.append("svg")
                .attr("width", author_width + author_margin.left + author_margin.right)
                .attr("height", author_height + author_margin.top + author_margin.bottom)
                .append("g")
                .attr("transform", "translate(" + author_margin.left + "," + author_margin.top + ")");

    /* add def for arrows */
    svg.append("svg:defs").selectAll("marker")
        .data(["end"])
        .enter().append("svg:marker") /* define arrow shape */
        .attr("id", String)
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 23.5)
        .attr("refY", -1.1)
        .attr("markerWidth", 4)
        .attr("markerHeight", 4)
        .attr("orient", "auto")
        .append("svg:path")
        .attr("d", "M0,-5L10,0L0,5");
}

function author_render(graph) {

    var links = [];
    for (var node in graph) {
        var node_data = graph[node];
        node_data.name = node;
        node_data.forEach(function (cur_val, index, array) {
            links.push({source: node_data,
                        target: graph[cur_val.to]});
        });
    }

    var nodes = d3.values(graph);
    
    var force = d3.layout.force()
                    .nodes(nodes)
                    .links(links)
                    .size([author_width, author_height])
                    .linkDistance(100)
                    .charge(-300)
                    .on("tick", tick)
                    .start();
 
    svg.select("g").remove();
    /* add the links and the arrows */
    var path = svg.append("svg:g").selectAll("path")
                    .data(force.links())
                    .enter().append("svg:path")
                    .attr("class", "link")
                    .attr("marker-end", "url(#end)");

    svg.selectAll(".node").remove();
    /* define the nodes */
    var node = svg.selectAll(".node")
                    .data(force.nodes())
                    .enter().append("g")
                    .attr("class", "node")
                    .call(force.drag);

    node.append("circle")
        .attr("r", 10);

    node.append("text")
        .attr("x", 14)
        .attr("dy", ".35em")
        .text(function(d) { return d.name; });

    function tick() {
        path.attr("d", function(d) {
            var dx = d.target.x - d.source.x,
                dy = d.target.y - d.source.y,
                dr = Math.sqrt(dx * dx + dy * dy);
            return "M" +
                d.source.x + "," +
                d.source.y + "A" +
                dr + "," + dr + " 0 0,1 " +
                d.target.x + "," +
                d.target.y;
        });

        node.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")"; });
    }

    for (var i = 0; i < 10; i++)
        force.tick();
}

function author_search(from, to) {
    send_request(author_ajax_site, { from: from, to: to, step: 4 },
                function(resp) {
                    console.log(resp);
                    author_render(resp);
                });
}

var debug_data = [
    {id: 1, title: "A", authors: ["a", "b", "c"], date: "2009"},
    {id: 2, title: "B", authors: ["e", "b", "c"], date: "2009"},
    {id: 3, title: "D", authors: ["a", "e", "c"], date: "2010"},
    {id: 4, title: "C", authors: ["a", "b", "d"], date: "2011"},
    {id: 5, title: "E", authors: ["a", "e"], date: "2013-02-03"},
];
