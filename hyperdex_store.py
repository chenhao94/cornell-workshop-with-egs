import hyperdex.admin
import hyperdex.client
import cPickle

def cleanup_space(a):
    a.rm_space("docs")
    a.rm_space("word_pairs")

def setup_space(a):
    a.add_space(" \
        space word_pairs \
        key string pair \
        attributes \
            list(string) doc_list \
    ")
    a.add_space(" \
        space docs \
        key int doc_id \
        attributes \
            string title, \
            string date, \
            string url, \
            list(string) authors \
    ")

def put_wpair(word_from, word_to, doc_list, c):
    c.async_put('word_pairs', '{0}_{1}'.format(word_from, word_to),
            {'doc_list': [cPickle.dumps(i, 2) for i in doc_list]})
def get_wpair(word_from, word_to, c):
    return [cPickle.loads(i) for i in \
            c.get('word_pairs', '{0}_{1}' \
                    .format(word_from, word_to))['doc_list']]
class DocInfo:
    def __init__(self, title, date, url, authors):
        self.title = title
        self.date = date
        self.url = url
        self.authors = [i for i in authors]
    def __repr__(self):
        return "<DocInfo with title:\"{0}\", date:\"{1}\", url:\"{2}\", authors:{3}" \
            .format(self.title, self.date, self.url, repr(self.authors))

def put_doc(doc_id, doc_info, c):
    c.async_put('docs', doc_id, \
            {'title': doc_info.title,
            'date': doc_info.date,
            'url': doc_info.url,
            'authors': doc_info.authors})

def get_doc(doc_id, c):
    doc = c.get('docs', doc_id)
    return DocInfo(doc['title'], doc['date'], doc['url'], doc['authors'])

if __name__ == '__main__':
    from ConfigParser import SafeConfigParser
    config_parser = SafeConfigParser()
    config_parser.read('server.cfg')
    
    host = config_parser.get('paper-repo', 'host') 
    port = config_parser.getint('paper-repo', 'port')

    a = hyperdex.admin.Admin(host, port)
    c = hyperdex.client.Client(host, port)
    cleanup_space(a)
    setup_space(a)
    put_wpair('this', 'is', [(1, 2), (2, 3), (3, 4)], c)
    print(get_wpair('this', 'is', c))
    put_doc(1, DocInfo('Ithaca Lure', '2012', 'google.com', ['alice', 'bob']), c)
    print(get_doc(1, c))
