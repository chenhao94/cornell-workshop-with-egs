from flask import Flask, render_template, request
import json
from ConfigParser import SafeConfigParser
import weaver.client as wclient
import hyperdex.client

config_parser = SafeConfigParser()
config_parser.read('server.cfg')

N_GRAM_WEAVER_HOST = config_parser.get('n-gram', 'host')
N_GRAM_WEAVER_PORT = config_parser.getint('n-gram', 'port')
N_GRAM_WEAVER_CONF = config_parser.get('n-gram', 'conf')

AUTHOR_WEAVER_HOST = config_parser.get('authorship', 'host')
AUTHOR_WEAVER_PORT = config_parser.getint('authorship', 'port')
AUTHOR_WEAVER_CONF = config_parser.get('authorship', 'conf')
AUTHOR_MAX_STEP = config_parser.getint('authorship', 'max_step') or 4

PAPER_REPO_HOST = config_parser.get('paper-repo', 'host')
PAPER_REPO_PORT = config_parser.getint('paper-repo', 'port')

HTTP_HOST = config_parser.get('http', 'host') or '0.0.0.0'
HTTP_PORT = config_parser.getint('http', 'port') or '80'
HTTP_DEBUG = config_parser.getboolean('http', 'debug') or False

n_gram_weaver = wclient.Client(N_GRAM_WEAVER_HOST, N_GRAM_WEAVER_PORT, N_GRAM_WEAVER_CONF)
author_weaver = wclient.Client(AUTHOR_WEAVER_HOST, AUTHOR_WEAVER_PORT, AUTHOR_WEAVER_CONF)
paper_repo = hyperdex.client.Client(PAPER_REPO_HOST, PAPER_REPO_PORT)

app = Flask(__name__)

class Page:
    def __init__(self, name, path):
        self.name = name
        self.path = path

class Site:
    def __init__(self, pages, brand):
        self.pages = pages
        self.brand = brand

page_spec = [['N-gram', 'n-gram'],
            ['Authorship', 'authorship']]

site = Site([Page(s[0], s[1]) for s in page_spec], "Project")

@app.route('/')
@app.route('/n-gram')
def n_gram():
    return render_template('n_gram_path.html', site=site, current_page='n-gram')

@app.route('/authorship')
def authorship():
    return render_template('authorship.html', site=site, current_page='authorship')

class DocInfo:
    def __init__(self, title, date, url, authors):
        self.title = title
        self.date = date
        self.url = url
        self.authors = []
        for author in authors:
            self.authors.append(author)

def find_doc(aid):
    doc = paper_repo.get("docs", aid);
    return DocInfo(doc['title'], doc['date'], doc['url'], doc['author'])

@app.route('/n-gram-query', methods=['POST'])
def n_gram_query():
    words = [word.encode("utf-8") for word in json.loads(request.form['words'])]
    print(words)
    try:
        res = map(lambda doc_id: [doc_id, find_doc(doc_id)],
                    n_gram_weaver.n_gram_path(words))
    except wclient.WeaverError as e:
        lst = []
    else:
        lst = [{'id':doc_id,
               'title':doc.title,
               'date':doc.date,
               'authors':doc.authors} \
                   for [doc_id,doc] in res]
    print('done')
    return json.dumps(lst)

def merge_subgraph(graph1, graph2):
    merged = {node: {edge.handle : edge for edge in graph1[node]} for node in graph1}
    for node in graph2:
        if not node in merged:
            merged[node] = {}
        medges = merged[node]
        for edge in graph2[node]:
            if not edge.handle in medges:
                medges[edge.handle] = edge
    return {node: [e for h, e in merged[node].iteritems()] for node in merged}

@app.route('/authorship-query', methods=['POST'])
def authorship_query():
    author_from = request.form['from'].encode("utf-8")
    author_to = request.form['to'].encode("utf-8")
    try:
        author_step = int(request.form['step'])
    except ValueError:
        author_step = AUTHOR_MAX_STEP
    author_step = min(AUTHOR_MAX_STEP, author_step)
    try:
        res, path = author_weaver.discover_paths(author_from, author_to,
                                                path_len=author_step)
    except wclient.WeaverError:
        res = {}
#    d = AUTHOR_MAX_STEP
#    for node in path:
#        res = merge_subgraph(author_weaver.discover_paths(
#            node, '', path_len = max(d, AUTHOR_MAX_STEP - d))[0], res)
#        d -= 1
    lst = {author: [{'to': e.end_node, 'doc_id': e.properties["doc_id"][0]} \
            for e in res[author]] for author in res}
    return json.dumps(lst)

if __name__ == "__main__":
    app.run(host=HTTP_HOST, port=HTTP_PORT, threaded=True, debug=HTTP_DEBUG)
